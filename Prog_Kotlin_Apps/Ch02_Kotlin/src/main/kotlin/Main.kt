package main

import com.shermannatrix.rnd.person.Person

fun main() {
	// Create a new person
	val brian = Person("Brian", "Truesby", 68.2, 33, true)
	println(brian)
	
	// Create another person
	val rose = Person("Rose", "Bushnell", 56.8, 32, true)
	println(rose)
	
	// Change Rose's last name
	rose.lastName = "Bushnell-Truesby"
	println(rose)
}