package com.shermannatrix.rnd.person

class Person (_firstName: String, _lastName: String, _height: Double, _age: Int, _hasPartner: Boolean) {
	
	var firstName: String = _firstName
	var lastName: String = _lastName
	//var fullName: String = combinedNames()
	var height: Double = _height
	var age: Int = _age
	var hasPartner: Boolean = _hasPartner
	
	// Set the full name when creating an instance
	/*init {
		updateName()
	}*/
	
	fun fullName(): String {
		return "$firstName $lastName"
	}
	
	override fun toString(): String {
		return fullName()
	}
}