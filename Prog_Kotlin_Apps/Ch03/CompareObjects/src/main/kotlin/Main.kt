import com.shermannatrix.rnd.person.Person

fun main(args: Array<String>) {
	// Create a new Person
	val brian = Person("Brian", "Truesby", 68.2, 33, true)
	println(brian)
	
	// Create another Person
	val rose = Person("Rose", "Bushnell", 56.8, 32, true)
	println(rose)
	
	// Change Rose's last name
	rose.lastName = "Bushnell-Truesby"
	println(rose)
	
	if (rose.equals(brian))
		println("It's the same person!")
	else
		println("Nope...not the same person")
	
	val second_brian = Person("Brian", "Truesby", 68.2, 33, true)
	if (second_brian.equals(brian))
		println("It's the same Brian!")
	else
		println("Nope...not the same Brian")
	
	val copy_of_brian = brian
	if (copy_of_brian.equals(brian))
		println("Now it's the same Brian!")
	else
		println("Nope...still not the same Brian")
	
<<<<<<< HEAD
=======
	val backward_brian = Person("Truesby", "Brian", 67.6, 42, false)
	if (backward_brian.equals(brian))
		println("Brian and Backward Brian are the same!")
	else
		println("Brian and Backward Brian are not the same!")
	
>>>>>>> main
	println("Brian hashcode: ${brian.hashCode()}")
	println("Rose hashcode: ${rose.hashCode()}")
	println("Second Brian hashcode: ${second_brian.hashCode()}")
	println("Copy of Brian hashcode: ${copy_of_brian.hashCode()}")
<<<<<<< HEAD
=======
	println("Backward Brian hashcode: ${backward_brian.hashCode()}")
>>>>>>> main
}