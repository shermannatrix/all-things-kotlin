import com.shermannatrix.rnd.person.Person

fun main(args: Array<String>) {
	// Create a new person
	val brian = Person("Brian", "Truesby", 68.2, 33)
	printPersonPartnerStatus(brian)
	
	// Create another person
	val rose = Person("Rose", "Bushnell", brian)
	printPersonPartnerStatus(rose)
	
	// Change Rose's last name
	rose.lastName = "Bushnell-Truesby"
	println(rose)
}

fun printPersonPartnerStatus(person: Person) {
	if (person.hasPartner) {
		println("$person has a partner: ${person.partner}")
	} else {
		println("$person does not have a partner.")
	}
}