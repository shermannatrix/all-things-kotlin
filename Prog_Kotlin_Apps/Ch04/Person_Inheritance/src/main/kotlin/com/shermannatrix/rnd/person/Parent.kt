package com.shermannatrix.rnd.person

class Parent (
	_firstName: String,
	_lastName: String,
	_height: Double = 0.0,
	_age: Int = 0,
	_child: Person) : Person (_firstName, _lastName, _height, _age) {
	var child: Person? = _child
}