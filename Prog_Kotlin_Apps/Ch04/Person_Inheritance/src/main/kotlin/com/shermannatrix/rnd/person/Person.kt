package com.shermannatrix.rnd.person

open class Person (_firstName: String,
              _lastName: String,
              _height: Double = 0.0,
              _age: Int = 0) {
	
	var firstName: String = _firstName
	var lastName: String = _lastName
	//var fullName: String = combinedNames()
	var height: Double = _height
	var age: Int = _age
	var partner: Person? = null
	val hasPartner: Boolean
		get() = (partner != null)

	constructor(_firstName: String,
		_lastName: String,
		_partner: Person) : this(_firstName, _lastName) {
			partner = _partner
		}
	
	// Set the full name when creating an instance
	/*init {
		updateName()
	}*/
	
	fun fullName(): String {
		return "$firstName $lastName"
	}
	
	override fun toString(): String {
		return fullName()
	}
	
	override fun hashCode(): Int {
		return (firstName.hashCode() * 28) + (lastName.hashCode() * 31)
	}
	
	override fun equals(other: Any?): Boolean {
		if (other is Person) {
			return (firstName.equals(other.firstName)) && (lastName.equals(other.lastName))
		} else {
			return false
		}
	}
}