import com.shermannatrix.rnd.person.Parent
import com.shermannatrix.rnd.person.Person

fun main(args: Array<String>) {
	// Create a new person
	val brian = Person("Brian", "Truesby", 68.2, 33)
	// printPersonPartnerStatus(brian)
	val rose = Person("Rose", "Bushnell", 64.3, 30)
	
	val mom = Parent("Cheryl", "Truesby", _child=brian)
	mom.children.add(rose)
	mom.children.add(Person("Barret", "Truesby"))
	
	println("Cheryl's kids: ${mom.children}")
	println(mom.children.filter { it.firstName.startsWith("B") })
}

fun printPersonPartnerStatus(person: Person) {
	if (person.hasPartner) {
		println("$person has a partner: ${person.partner}")
	} else {
		println("$person does not have a partner.")
	}
}