fun main(args: Array<String>) {
	val rockBands = mutableListOf("The Black Crowes", "Led Zeppelin", "The Beatles")
	println(rockBands)
	
	println("rockBands is a ${rockBands::class.simpleName}")
	
	rockBands.add("The Rolling Stones")
	println(rockBands)
	
	// Removing an item from the list
	rockBands.removeAt(2)
	println(rockBands)
	
	// Remember that lists are zero-based!
	println("The first band is ${rockBands.get(0)}")
	
	// Use array notation
	println("The second band is ${rockBands[1]}")
	
	// Add to a specific index
	rockBands.add(1, "The Eagles")
	println("The second band is ${rockBands[1]}")
	println(rockBands)
	
	val mixedThings = mutableListOf("Electric Guitar", 42)
	println(mixedThings)
	
	val item = mixedThings[0]
	if (item is String) {
		println("Value: $item")
	} else if (item is Int) {
		println(3 + item)
	}
}