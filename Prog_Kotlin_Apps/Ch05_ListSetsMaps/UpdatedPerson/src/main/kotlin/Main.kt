import com.shermannatrix.rnd.music.*
import com.shermannatrix.rnd.person.*

fun main(args: Array<String>) {
	val rockBands = mutableListOf<Band>(
		Band(
			"The Beatles",
			_singer = Person("John", "Lennon"),
			_guitarist = Person("George", "Harrison"),
			_bassist = Person("Paul", "McCartney"),
			_drummer = Person("Ringo", "Starr")
		),
		Band(
			"The Rolling Stones",
			_singer = Person("Mick", "Jagger"),
			_guitarist = Person("Keith", "Richards"),
			_bassist = Person("Ronnie", "Wood"),
			_drummer = Person("Charlie", "Watts")
		),
		Band(
			"The Black Crowes",
			_singer = Person("Chris", "Robinson"),
			_guitarist = Person("Rich", "Robinson"),
			_drummer = Person("Steve", "Gorman"),
			_bassist = Person("Johnny", "Colt")
		)
	)
	
	println(rockBands)
	println("rockBands is a ${rockBands::class.simpleName}")
	println("There are ${rockBands.size} bands in the list.")
	rockBands.add(
		Band(
			"The Black Keys",
			_singer = Person("Dan", "Auerbach"),
			_guitarist = Person("Steve", "Marion"),
			_drummer = Person("Patrick", "Carney"),
			_bassist = Person("Richard", "Swift")
		)
	)
	
	println("Now there are ${rockBands.size} bands in the list.")
	println("The second band is ${rockBands[1]}")
	
	// loop through the bands and output some details
	println("We will loop through the bands to print the singer's names...")
	val bandsIterator = rockBands.iterator()
	while (bandsIterator.hasNext()) {
		var band = bandsIterator.next()
		println("The lead singer for ${band.name} is ${band.singer}")
	}
	
//	println("\r\nLet's start looking at using sets...")
//	val bandSet = setOf<Band>(rockBands[0], rockBands[1], rockBands[2], rockBands[3])
//	println("${bandSet::class.simpleName}")
//
//	println(rockBands)
//	println(bandSet)
	
	println("\r\nUsing hashSetOf<Band> this time...")
	val bandSet = hashSetOf<String>(rockBands[0].name, rockBands[1].name, rockBands[2].name, rockBands[3].name)
	println("${bandSet::class.simpleName}")
	
	println(rockBands)
	println(bandSet)
	
	println("\r\nLet's sort out list of bands...")
	for (band in rockBands.sortedBy { band: Band -> band.name })
		println(band.name)
	
//	println("\r\nUsing iterators to remove items from a list...")
//	var iterator = rockBands.iterator()
//	while (iterator.hasNext()) {
//		var band = iterator.next()
//		if (band.name.contains("black", ignoreCase = true)) {
//			iterator.remove()
//		}
//	}
//
//	for (band in rockBands)
//		println(band.name)
	
	println("\r\nWe'll start doing some filtering of the bands...")
	var bandsInBlack = mutableListOf<Band>()
	println("Before filtering...")
	for (band in rockBands) {
		println(band.name)
		if (band.name.contains("Black", ignoreCase = true)) {
			bandsInBlack.add(band)
		}
	}
	
	println("\r\nAfter filtering...")
	for (band in bandsInBlack)
		println(band.name)
}