import com.shermannatrix.rnd.person.Parent
import com.shermannatrix.rnd.person.Person

fun main(args: Array<String>) {
	// Create a new person
	val brian = Person("Brian", "Truesby", 68.2, 33)
	printPersonPartnerStatus(brian)
	
	// Create another person
	val rose = Person("Rose", "Bushnell", brian)
	printPersonPartnerStatus(rose)
	
	// Change Rose's last name
	rose.lastName = "Bushnell-Truesby"
	println(rose)
	
	val mom = Parent("Cheryl", "Truesby", _child=brian)
	mom.child = rose
	
	val somePeople = mutableListOf<Person>(brian, rose)
	println(somePeople)
	
	// Subclasses of Person are still a Person!
	somePeople.add(mom)
	println(somePeople)
	
	// Create a person and add it in one step
	somePeople.add(Person("Brett", "McLaughlin"))
	println(somePeople)
	
	// Only some Person instances can be added
	// somePeople.add(32)
}

fun printPersonPartnerStatus(person: Person) {
	if (person.hasPartner) {
		println("$person has a partner: ${person.partner}")
	} else {
		println("$person does not have a partner.")
	}
}