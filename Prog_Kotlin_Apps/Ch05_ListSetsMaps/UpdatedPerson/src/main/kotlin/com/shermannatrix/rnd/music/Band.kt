package com.shermannatrix.rnd.music

import com.shermannatrix.rnd.person.*

class Band (_name: String, _singer: Person, _guitarist: Person,
	_drummer: Person, _bassist: Person) {
	var name: String = _name
	var singer: Person = _singer
	var guitarist: Person = _guitarist
	var drummer: Person = _drummer
	var bassist: Person = _bassist
}