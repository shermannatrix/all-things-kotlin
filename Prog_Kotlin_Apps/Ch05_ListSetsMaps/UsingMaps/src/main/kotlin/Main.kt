fun main(args: Array<String>) {
	var statusCodes = mapOf(
		200 to "OK",
		202 to "Accepted",
		402 to "Payment Required",
		404 to "Not Found",
		500 to "Internal Server Error")
	
	println("Keys: ${statusCodes.keys}")
	println("Values: ${statusCodes.values}")
	
	println("202 has an associated value of '${statusCodes[202]}'")
	
	if (statusCodes.contains(404)) {
		println("Message: ${statusCodes[404]}")
	} else {
		println("No message for 404.")
	}
	
	println(statusCodes.getOrDefault(400, "Default Value"))
}