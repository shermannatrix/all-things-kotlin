import com.shermannatrix.rnd.person.Child
import com.shermannatrix.rnd.person.Parent
import com.shermannatrix.rnd.person.Person

fun main(args: Array<String>) {
	// Add some people
	val brian = Person("Brian", "Truesby", 68.2, 33)
	val rose = Person("Rose", "Bushnell", brian)
	val leigh = Person("Leigh", "McLaughlin")
	
	// Add some parents
	val cheryl = Parent("Cheryl", "Truesby", _child=brian)
	val shirley = Parent("Shirley", "Greathouse", _child=leigh)
	
	// Add some children
	val quinn = Child("Quinn", "Greathouse", _parent=shirley)
	val laura = Child("Laura", "Jordan", _parent=shirley)
	
	val momList: List<Parent> = listOf(cheryl, shirley)
	val familyReunion: List<Person> = momList
}