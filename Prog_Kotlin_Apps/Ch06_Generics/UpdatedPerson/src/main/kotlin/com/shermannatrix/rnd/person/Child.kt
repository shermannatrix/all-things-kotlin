package com.shermannatrix.rnd.person

import com.shermannatrix.rnd.person.Person

class Child (_firstName: String, _lastName: String,
             _height: Double = 0.0, _age: Int = 0,
			_parents: Set<Person>) : Person(_firstName, _lastName, _height, _age) {
	var parents: MutableSet<Person> = mutableSetOf()
	
	init {
		parents.addAll(_parents)
	}
	
	constructor(_firstName: String, _lastName: String, _parent: Person) :
			this(_firstName, _lastName, _parents = setOf(_parent))
}