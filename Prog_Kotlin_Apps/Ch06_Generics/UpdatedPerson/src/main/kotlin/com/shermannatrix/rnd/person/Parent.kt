package com.shermannatrix.rnd.person

class Parent (
	_firstName: String,
	_lastName: String,
	_height: Double = 0.0,
	_age: Int = 0,
	_children: Set<Person>) : Person (_firstName, _lastName, _height, _age) {
	var children: MutableSet<Person> = _children.toMutableSet()
	
	constructor(_firstName: String, _lastName: String, _child: Person) :
			this(_firstName, _lastName, _children = setOf(_child))
}