import kotlin.random.Random

fun main() {
    var theNumber = Random.nextInt(0, 1000)

    print("Guess my number: ")
    val guess = readLine()!!

    if (guess.toInt() == theNumber) {
        println("You got it!")
    } else {
        println("Sorry, you missed it.")
    }
}