import kotlin.random.Random

fun main() {
    var theNumber = Random.nextInt(0, 1000)

    print("Guess my number: ")
    val guess = readLine()!!

    print("The number you guessed is ")
    var hasRightNumber = if (guess.toInt() == theNumber) {
        println("right!")
        true
    } else {
        println("wrong.")
        false
    }
    println(hasRightNumber)

    var anotherNumber = Random.nextInt(0, 1000)
    var even = false
    when {
        (anotherNumber % 2 == 0) -> println("The number is even!")
        else -> println("The numbner is odd!")
    }

    when {
        (anotherNumber % 2 == 0) -> println("The number is divisible by 2!")
        (anotherNumber % 3 == 0) -> println("The number is divisible by 3!")
        (anotherNumber % 5 == 0) -> println("The number is divisible by 5!")
        (anotherNumber % 7 == 0) -> println("The number is divisible by 7!")
        (anotherNumber % 11 == 0) -> println("The number is divisible by 11!")
        else -> println("This thing is hardly divisible by anything!")
    }

    println("The actual number was $anotherNumber")

    var foo: Long = 23

    // println(isPositiveInteger(foo))

    val numbers = listOf(0, 1, 2, 3, 4)
    for (item in numbers) {
        println(item)
    }

    val ar = intArrayOf(1, 2, 3, 4, 5, 6, 7, 8)
    var i: Int
    var x: Int

    var counter: Int = 1
    for (i in ar.indices) {
        x = ar[i]
        println("Item ${counter}: ${x} is at index ${i}")
        counter++
    }
}