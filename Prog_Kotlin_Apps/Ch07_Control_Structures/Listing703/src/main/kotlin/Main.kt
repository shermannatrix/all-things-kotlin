import kotlin.random.Random

fun main() {
    var theNumber = Random.nextInt(0, 1000)

    print("Guess my number: ")
    val guess = readLine()!!

    print("The number you guessed is ")
    var hasRightNumber = if (guess.toInt() == theNumber) {
        println("right!")
        true
    } else {
        println("wrong.")
        false
    }

    println(hasRightNumber)

    var anotherNumber = Random.nextInt(0, 1000)

    var even = false
    when {
        (anotherNumber % 2 == 0) -> println("The number is even!")
    }

    when {
        (anotherNumber % 2 == 0) -> println("The number is divisible by 2!")
        (anotherNumber % 3 == 0) -> println("The number is divisible by 3!")
        (anotherNumber % 5 == 0) -> println("The number is divisible by 5!")
        (anotherNumber % 7 == 0) -> println("The number is divisible by 7!")
        (anotherNumber % 11 == 0) -> println("The number is divisible by 11!")
        else -> println("This thing is hardly divisible by anything!")
    }

    if (anotherNumber % 2 == 0) {
        println("The number is divisible by 2!")
    } else if (anotherNumber % 3 == 0) {
        println("The number is divisible by 3!")
    } else if (anotherNumber % 5 == 0) {
        println("The number is divisible by 5!")
    } else if (anotherNumber % 7 == 0) {
        println("The number is divisible by 7!")
    } else if (anotherNumber % 11 == 0) {
        println("The number is divisible by 11!")
    } else {
        println("This thing is hardly divisible by anything!")
    }

    println("The actual number was $anotherNumber")

    var foo: Long = 23

    val numbers = listOf(0, 1, 2, 3, 4)
    for (item in numbers) {
        println(item)
    }

    val ar = intArrayOf(1, 2, 3, 4, 5, 6, 7, 8)
    var i: Int
    var x: Int

    var counter: Int = 1
    for (i in ar.indices) {
        x = ar[i]
        println("Item ${counter}: ${x} is at index ${i}")
        counter++
    }

    x = 1
    while (x < 6) {
        println(x)
        x++
    }

    for (x in 1..5) {
        println(x)
    }

    outer_loop@ for (i in 1..10) {
        var low = 1
        var high = 100

        while (low < high) {
            var foundPrime = false

            prime_loop@ for (i in 2..low / 2) {
                if (low % i == 0) {
                    foundPrime = true
                    break@outer_loop
                }
            }

            if (!foundPrime)
                print("$low ")

            low++
        }
        println()
    }

    var condition: Boolean = Random.nextBoolean()
    while (condition) {
        println("Still true!")
        condition = Random.nextBoolean()
    }
    println("Not true any more!")

    while (false) {
        println("Code that never runs!")
    }

    condition = false
    do {
        println("Gonna run at least once!")
    } while (condition)

    condition = true
    while (condition) {
        // This line actually will not run now
        println("Gonna run at least once")
        condition = false
    }

    var emptyList: MutableList<String> = mutableListOf()

    do {
        emptyList.addAll(someStrings())
    } while (emptyList.size < 100)

    println(emptyList)
}