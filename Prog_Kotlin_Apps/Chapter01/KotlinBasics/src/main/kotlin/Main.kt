data class User (var firstName: String, var lastName: String) {
	var fullName: String
	
	// Set the full name when creating an instance
	init {
		fullName = "$firstName $lastName";
	}
	
	override fun toString(): String {
		return fullName
	}
}

fun main(args: Array<String>) {
	val brian = User("Brain", "Truesby")
	val rose = User("Rose", "Bushnell")
	
	val attendees: MutableList<User> = mutableListOf(brian, rose)
	
	attendees.forEach {
		user -> println(user)
	}
	
	// Change Rose's last name
	rose.lastName = "Bushnell-Truesby"
	println(rose)
}